<!DOCTYPE html>
<html>
    <head>
        <title>INF115 - CE3</title>
    </head>
    <body>
        <h1>INF115 - Compulsory exercise 3</h1>

            <?php
            /*
                Database configuration
            */

            // Connection parameters
            /*
                Information here is as required by the assignment, not the actual location.
                I am also aware that this setup is very vulnerable to hackers, but data
                security was not within the scope of the subject; as well as the setup
                code was provided by lectors.
            */
            $host 		= 'localhost';
            $user 		= 'root';
            $password 	= '';
            $db 		= 'bysykkel';

            // Connect to the database
            $conn = mysqli_connect($host, $user, $password, $db);

            // Connection check
            if(!$conn) {
                exit('Error: Could not connect to the database.');
            }

            // Set the charset
            mysqli_set_charset($conn, 'utf8');
            ?>

        <h1> Task 1 </h1>
            <h2> a) </h2>

            <?php
            echo "<i>[REDACTED]<br>295967</i>";
            ?>
            

            <h2> b) </h2>
            <form action="?" method="post">
                Please enter a phone number and an email address
                <br>
                <label for="email">Email Address:</label>
                <br>
                <input type="text" name="email">
                <br>
                <label for="phone">Phone Number:</label>
                <br>
                <input type="text" name="phone">
                <br>
                <br>
                <input type="reset" value="Reset">   <input type="submit" value="Submit">
            </form>

            <?php
            
            if ( isset($_POST["phone"], $_POST["email"]) && !empty($_POST["phone"]) && !empty($_POST["email"])) {
                
                echo $_POST["email"];
                echo "<br>";
                echo $_POST["phone"];

            }

            ?>

            
            <h2> c) </h2>
            <?php
            // cheat sheet used https://cheatography.com/davechild/cheat-sheets/regular-expressions/
            if ( isset($_POST["phone"], $_POST["email"]) && !empty($_POST["phone"]) && !empty($_POST["email"])) {
                
                echo $_POST["email"];
                if( preg_match("/\A[[:upper:]][[:word:]]+\.[[:upper:]][[:word:]]+\@[[:word:]]+\.[[:word:]]+\Z/", $_POST["email"]) ) {
                    echo " - Valid";
                }
                else {
                    echo " - Not valid";
                }

                echo "<br>";

                echo $_POST["phone"];
                if( preg_match("/\A\d{8}$\Z/", $_POST["phone"]) ) {
                    echo " - Valid";
                }
                else {
                    echo " - Not valid";
                }
            }
            ?>
            
        
        <h1> Task 2 </h1>
            <h2> a) </h2>
            <?php
            try {
                $sql = "UPDATE users SET name='Tore Antonsen' WHERE user_id=20";
                mysqli_query($conn, $sql);
                echo "Name changed";
            }
            catch (Exception $e) {
                //I would not normaly return any message, because a hypotethical
                //hacker could probe it to hack the system
                echo "No user with this user_id";
            }

            echo "<br>";

            try {
                $sql = "UPDATE users SET user_id=21 WHERE user_id=20";
                mysqli_query($conn, $sql);
                echo "user_id changed";
            }
            catch (Exception $e) {
                echo "user_id not changed";

            }
            ?>

            <h2> b) </h2>

            
            <table>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Station</th>
                </tr>
                <?php
                $sql = "SELECT b.bike_id as Bike_ID, b.name as Bike_Name, s.name as Station_Name FROM (SELECT * FROM bikes WHERE status='ACTIVE') b LEFT JOIN stations s ON b.station_id=s.station_id;";
                $result = mysqli_query($conn, $sql);
                $row = mysqli_fetch_assoc($result);

                while ($row) {

                    $id = $row["Bike_ID"];
                    $bname = $row["Bike_Name"];
                    $sname = $row["Station_Name"];

                    echo "<tr><td>$id</td><td>$bname</td><td>$sname</td></tr>";
                    $row = mysqli_fetch_assoc($result);
                }
                ?>

            </table>

            <h2> c) </h2>
            <?php
            $sql = "SELECT sum(row_count) AS num_rows FROM (SELECT count(*) AS row_count FROM stations UNION ALL SELECT count(*) AS row_count FROM subscriptions UNION ALL SELECT count(*) AS row_count FROM users UNION ALL SELECT count(*) AS row_count FROM bikes UNION ALL SELECT count(*) AS row_count FROM trips) AS b;";

            $result = mysqli_query($conn, $sql);

            $count = mysqli_fetch_assoc($result);
            $num_rows = $count["num_rows"];

            echo "$num_rows";


            ?>
           

        <h1> Task 3 </h1>
            <h2> a) </h2>
            <table>
                <tr>
                    <th>Name</th>
                    <th>Subscription start time</th>
                    <th># of Subscriptions</th>
                </tr>
                <?php
                $sql = "SELECT u.name, s.start_time, s.subscriptions 
                FROM (
                    SELECT user_id, start_time, count(user_id) as subscriptions 
                    FROM subscriptions 
                    WHERE year(start_time) >= 2020 
                    GROUP BY user_id
                    ) s JOIN users u ON s.user_id=u.user_id;";
                $result = mysqli_query($conn, $sql);
                $row = mysqli_fetch_assoc($result);

                while ($row) {

                    $name = $row["name"];
                    $start = $row["start_time"];
                    $num_sub = $row["subscriptions"];

                    echo "<tr><td>$name</td><td>$start</td><td>$num_sub</td></tr>";
                    $row = mysqli_fetch_assoc($result);
                }
                
                ?>
            </table>
                

            <h2> b) </h2>
            <table>
                <tr>
                    <th>Bike name</th>
                    <th>Bike status</th>
                    <th>Last start station</th>
                </tr>
                <?php
                $sql = "SELECT 
                b.name AS Name, b.status AS Status, s.name AS Station
            FROM
                (SELECT 
                    bike_id, name, status
                FROM
                    bikes
                WHERE
                    station_id = (SELECT 
                            station_id
                        FROM
                            bikes
                        GROUP BY station_id
                        HAVING COUNT(station_id) = (SELECT 
                                COUNT(station_id) AS num
                            FROM
                                bikes
                            GROUP BY station_id
                            ORDER BY num DESC
                            LIMIT 1))) AS b
                    INNER JOIN
                trips AS t ON b.bike_id = t.bike_id
                    LEFT JOIN
                stations AS s ON t.start_station = s.station_id;";
                $result = mysqli_query($conn, $sql);
                $row = mysqli_fetch_assoc($result);

                while ($row) {

                    $name = $row["Name"];
                    $status = $row["Status"];
                    $station = $row["Station"];

                    echo "<tr><td>$name</td><td>$status</td><td>$station</td></tr>";
                    $row = mysqli_fetch_assoc($result);
                }
                
                ?>
            </table>

            <h2> c) </h2>
            <table>
                <tr>
                    <th>user_id</th>
                    <th>Name</th>
                    <th>2018</th>
                    <th>2019</th>
                    <th>2020</th>
                    <th>2021</th>
                </tr>
                <?php
                $sql = "SELECT u.user_id, u.name, COALESCE(a.year2018, 0) as '2018', COALESCE(b.year2019, 0) as '2019', COALESCE(c.year2020, 0) as '2020', COALESCE(d.year2021, 0) as '2021'
                FROM users u 
                LEFT JOIN (
                    SELECT user_id, count(user_id) as year2018
                    FROM subscriptions
                    WHERE year(start_time) = 2018
                    GROUP BY user_id) a
                ON u.user_id=a.user_id
                LEFT JOIN (
                    SELECT user_id, count(user_id) as year2019
                    FROM subscriptions
                    WHERE year(start_time) = 2019
                    GROUP BY user_id) b
                ON u.user_id=b.user_id
                LEFT JOIN (
                    SELECT user_id, count(user_id) as year2020
                    FROM subscriptions
                    WHERE year(start_time) = 2020
                    GROUP BY user_id) c
                ON u.user_id=c.user_id
                LEFT JOIN (
                    SELECT user_id, count(user_id) as year2021
                    FROM subscriptions
                    WHERE year(start_time) = 2021
                    GROUP BY user_id) d
                ON u.user_id=d.user_id
                GROUP BY u.user_id;";
                $result = mysqli_query($conn, $sql);
                $row = mysqli_fetch_assoc($result);

                while ($row) {
                    $id = $row["user_id"];
                    $name = $row["name"];
                    $y2018 = $row["2018"];
                    $y2019 = $row["2019"];
                    $y2020 = $row["2020"];
                    $y2021 = $row["2021"];

                    echo "<tr><td>$id</td><td>$name</td><td>$y2018</td><td>$y2019</td><td>$y2020</td><td>$y2021</td></tr>";
                    $row = mysqli_fetch_assoc($result);
                }
                
                ?>
            </table>
        
        <h1> Task 4 </h1>
        <!-- Write your solution to 4 here -->
            <form action="?" method="post">
                <label for="email">Subscription category:</label>
                <br>
                <select name="types">
                    <option value="'Day'">Day</option>
                    <option value="'Week'">Week</option>
                    <option value="'Month'">Month</option>
                    <option value="'Year'">Year</option>
                </select>
                <br>
                <input type="submit" value="Submit">
            </form>
            
            <?php
            if(isset($_POST['types'])) {

                echo "<table>";
                    echo "<tr>";
                        echo "<th>Subscription Type</th>";
                        echo "<th># of Subscriptions</th>";
                        echo "<th>& of all Subscriptions</th>";
                    echo "</tr>";



                    //source https://stackoverflow.com/a/37303835
                    $current_type = $_POST["types"];
                    $sql = "SELECT type AS Type, count(type) AS Count, concat(round(count(type)*100/(
                        SELECT count(*) FROM subscriptions
                        )), '%') as Percent
                    FROM subscriptions
                    WHERE type=$current_type
                    GROUP BY type;";

                    $result = mysqli_query($conn, $sql);
                    $row = mysqli_fetch_assoc($result);

                    while ($row) {

                        $type = $row["Type"];
                        $count = $row["Count"];
                        $percent = $row["Percent"];

                        echo "<tr><td>$type</td><td>$count</td><td>$percent</td></tr>";
                        $row = mysqli_fetch_assoc($result);
                    }


                echo "</table>";   
                }
            
            ?>
            



    </body>
</html>
